
/**
 * @author Sebastien Gerard <sebeuu@gmail.com>
 * @see http://tsstatus.sebastien.me/
 * 
 **/

function tsstatusconnect(host, port, channel){
	var command = "ts3server://" + host + "/?port=" + port;
	var nick = "";
	
	if(document.getElementById("tsstatusNick") != null) nick = document.getElementById("tsstatusNick").value;
	if(nick != ""){
		command += "&nickname=" + nick;
		var dateExpire = new Date;
		dateExpire.setMonth(dateExpire.getMonth()+1);
		document.cookie = escape("tsstatus_" + host) + "=" + escape(nick) + "; expires=" + dateExpire.toGMTString();
	}
	if(channel != undefined){
		command += "&channel=" + channel;
	}
	var popup = window.open(command);
	popup.close();
}
